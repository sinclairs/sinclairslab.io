+++
title = "Home"
+++

I am a researcher in haptics and audio, and a software developer at heart.  A couple of
years ago I made a graceful exit from academia to flirt with applications of my skillset
in the industry, and have since been doing contract work on various projects.  I am always
on the lookout for interesting [projects](projects/) at the intersection of media
(visual/audio/haptic signals) and machine learning.

I've lived in 4 countries in the last 7 years, but my current residence is in Utrecht,
Netherlands.

Latest work:

* For [NavInfo Europe](https://www.navinfo.eu/), development of scenario extraction
  methods for highway driving using computer vision.  Road reconstruction, vehicle
  detection, and event identification.  The work is a mix of modern ML and classic CV
  methods, using technologies such as PyTorch, OpenCV, and Scikit-Learn.

Previous highlights:

* Measurement system for fingerpad skin deformation under normal
  loading (for [MSLab](http://mslab.es/).
* Implementation and optimization of a neural network-driven real time
  audio filter by fast convolution (for [Enosis
  VR](http://enosis.io/)), work that is now part of an HP product.
* Design and implementation of real-time rendering methods for
  ultrasound haptics (for [MSLab](http://mslab.es/)).
* Encoding and real time synthesis of physically-based audio using
  neural networks in [Sounderfeit](projects/sounderfeit/) (personal
  project while at [Inria Chile](https://inria.cl))
* Development of [new physics
  back-end](https://github.com/siconos/gazebo-siconos) using Inria's
  [Siconos](https://siconos.gforge.inria.fr) for the
  [Gazebo](http://gazebosim.org) robotics simulator &mdash; and some
  major enhancements to Siconos mechanics in the process, including
  support for friction and detents within joint constraints (for
  [Inria Chile](https://inria.cl)).

Some older work, projects in which I was a *prime mover*:

* [DIMPLE](https://radarsat1.github.io/dimple/) is a 3D haptic
  environment intended to be used in conjunction with creativity tools
  such as PureData, Max/MSP, SuperCollider, etc.
* [libmapper](http://libmapper.org) is a library for mapping Open
  Sound Control messages between devices on a local network in a
  decentralized manner.  (Co-developed originally with [Joseph
  Malloch](https://josephmalloch.wordpress.com/), work [continues by
  the IDMIL team](http://www-new.idmil.org/project/libmapper/) today!)
* Music systems, [LoopDub](http://loopdub.sourceforge.net/) is among
  several systems I developed over the years to play live techno
  music, all used on stage at some point.
* Investigated methods for adapting physical *bowed string* models for
  force feedback haptics, including an evaluation of [velocity
  estimators](https://www.researchgate.net/publication/264795207_Velocity_Estimation_Algorithms_for_Audio-Haptic_Simulations_Involving_Stick-Slip)
  for friction-driven haptics ([PhD
  work](https://www.researchgate.net/publication/270818612_Velocity-based_Audio-Haptic_Interaction_With_Real-Time_Digital_Acoustic_Models)).

Open source:

I am the maintainer of the popular [liblo](http://liblo.sf.net)
library for Open Sound Control, a co-maintainer for the
[RtMidi/RtAudio](https://github.com/thestk) libraries with Gary
Scavone, and I also maintain a small handful of Debian packages on the
[Science team](https://wiki.debian.org/DebianScience), including
[Keras](https://tracker.debian.org/pkg/keras) and
[Siconos](https://tracker.debian.org/pkg/siconos).

My [CV can be found here](cv/sinclair_cv2022.pdf) (PDF), please [get
in touch](mailto:radarsat1@gmail.com) if you've got some great ideas!
