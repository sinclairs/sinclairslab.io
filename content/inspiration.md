+++
title = "Inspiration"
+++

Here are some articles that I find inspiring.  I hope to do blog posts
soon on *what* they are and *why* I find them interesting.

J. P. Fiene and K. J. Kuchenbecker, "Shaping event-based haptic transients via
  an improved understanding of real contact dynamics," in *Proc. World
  Haptics*, pp. 170--175, IEEE, 2007.

V. Kumar, E. Todorov, and S. Levine, "Optimal control with learned local
  models: Application to dexterous manipulation," in *Int. Conf. Robotics
  and Automation (ICRA)*, pp. 378--383, IEEE, 2016.

T. G. Thuruthel, E. Falotico, F. Renda, and C. Laschi, "Learning dynamic
  models for open loop predictive control of soft robotic manipulators,"
  *Bioinspiration & Biomimetics*, vol. 12, no. 6, p. 066003, 2017.

E. Maestre, M. Blaauw, J. Bonada, E. Guaus, and A. Pérez,
  "Statistical modeling of bowing control applied to violin sound
  synthesis," *IEEE Transactions on Audio, Speech, and Language
  Processing*, vol. 18, no. 4, pp. 855--871, 2010.

A. Pfalz and E. Berdahl, "Toward inverse control of physics-based sound
  synthesis," *arXiv preprint arXiv:1706.09551*, 2017.

Tindale and G. Tzanetakis, "The {E-Drum}: A case study for machine
  learning in new musical controllers," *J. Interdisciplinary Music
  Studies*, vol. 6, pp. 115--136, 2012.

G. Percival, N. Bailey, and G. Tzanetakis, "Physical modelling and
  supervised training of a virtual string quartet," in *Proc. ACM
  Multimedia*, 2013.

M. Schmidt and H. Lipson, "Distilling free-form natural laws from
experimental data," *Science*, vol. 324, no. 5923, pp. 81--85, 2009.

S. Coros et al. ["Computational Design of Animated Mechanical Characters."](https://s3-us-west-1.amazonaws.com/disneyresearch/wp-content/uploads/20140804211255/CDMC1.pdf)
APS Meeting Abstracts. 2014.
[Video](https://www.youtube.com/watch?v=DfznnKUwywQ)
