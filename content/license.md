+++
title= "License"
+++

Content of this page is copyright 2017 Stephen Sinclair according to
the details of the Creative Commons Attribution-ShareAlike 4.0
International Public License. Full text here:

https://creativecommons.org/licenses/by-sa/4.0/legalcode
