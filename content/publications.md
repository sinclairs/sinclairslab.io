+++
title = "Publications"
+++

Here is a list of my journal publications in latest-to-oldest order.
Links are directly to PDF whenever possible, or to a landing page
related to the paper.  Direct PDF links are noted as such.

### Recent publications

H. Barreiro, S. Sinclair, and M. A. Otaduy. ["Path Routing Optimization for STM Ultrasound
Rendering."](https://ieeexplore.ieee.org/abstract/document/9007477) In *Transactions on
Haptics*, 13.1 (2020), pp. 45--51.

H. Barreiro, S. Sinclair, and M. A. Otaduy. ["Ultrasound Rendering of Tactile Interaction
with Fluids."](https://www.gmrv.es/Publications/2019/BSO19) In [*Proc. World Haptics
Conference*](https://ieeexplore.ieee.org/abstract/document/8816137/), pp. 521--526. IEEE,
2019.

S. Sinclair, ["Sounderfeit: cloning a physical model using a
  conditional adversarial
  autoencoder,"](https://arxiv.org/abs/1806.09617) in [*Revista Música
  Hodie*](https://www.revistas.ufg.br/musica/article/view/53570), 18.1
  (2018), pp. 44--60. <span style="font-size:8pt">(Extended version of
  the corresponding conference paper selected for open access
  publication.  Note: the ArXiv version is identical except for
  corrected equations and formatting wrt the pdf produced by the
  journal.)</span>

S. Sinclair, ["Sounderfeit: cloning a physical model with conditional
  adversarial autoencoders,"](https://gitlab.com/sinclairs/sounderfeit/raw/72851c4ebdb785eb3da4c5ae6a86d3402cb1c73d/doc/Sinclair2017_sounderfeit.pdf)
  in *Proc. Brazilian Symp. on Comp. Music*, SBC, 2017.

### Journal publications

S. Bochereau, S. Sinclair, and V. Hayward, ["Perceptual constancy in
  the reproduction of virtual tactile textures with surface displays,"
  (pdf)](http://cim.mcgill.ca/~haptic/pub/SB-ET-AL-TAP-17.pdf) *ACM
  Transactions on Applied Perception (TAP)* 15, no. 2 (2018): 10.

J. Malloch, S. Sinclair, and M. M. Wanderley, ["Generalized
  multi-instance control mapping for interactive media
  systems,"](https://ieeexplore.ieee.org/abstract/document/8259406/)
  *IEEE Multimedia*, Jan. 2018.  *IEEE MultiMedia* 25, no. 1 (2018): 39--50.

C. Pacchierotti, S. Sinclair, M. Solazzi, A. Frisoli, V. Hayward, and
  D. Prattichizzo, ["Wearable haptic systems for the fingertip and the
  hand: taxonomy, review, and perspectives,"](http://ieeexplore.ieee.org/abstract/document/7922602/) *IEEE Transactions on Haptics*, 2017.

J. Malloch, S. Sinclair, and M. M. Wanderley, ["Distributed tools for
  interactive design of heterogeneous signal networks,"](link.springer.com/article/10.1007/s11042-014-1878-5)
  *Multimedia Tools and Applications*, vol. 74, no. 15, pp. 5683--5707, 2015.

S. Sinclair, M. M. Wanderley, and V. Hayward, ["Velocity estimation
  algorithms for audio-haptic simulations involving stick-slip," (pdf)](http://citeseerx.ist.psu.edu/viewdoc/download;jsessionid=76C8BFC07517D26B0A3BFDF1EA687D7F?doi=10.1.1.701.3872&rep=rep1&type=pdf)
  *Tran. Haptics*, vol. 7, no. 4, pp. 533--544, 2014.

S. Sinclair and M. M. Wanderley, ["A run-time programmable simulator to
  enable multi-modal interaction with rigid-body systems," (pdf)](https://pdfs.semanticscholar.org/a569/300ded4ffaa77101e58e4aac624dcc38bf48.pdf)
  *Interacting with Computers*, vol. 21, no. 1-2, pp. 54--63, 2008.

### Some bonus conference publications:

X. Pestova, E. Donald, H. Hindman, J. Malloch, M. T. Marshall, F. Rocha,
  S. Sinclair, D. A. Stewart, M. M. Wanderley, and S. Ferguson, ["The
  CIRMMT/McGill digital orchestra project," (pdf)](http://xeniapestova.com/DORCH_20090528b.pdf)
  in *Proc. ICMC*, 2009.
