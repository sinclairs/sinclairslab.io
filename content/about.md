+++
date = "2017-11-27"
title = "About"
+++

_To hear and touch_

I am a researcher and developer.  I am interested in **mechanisms**,
how to **control** them, how **we** control them, the **signals** they
generate, and how we **perceive** these through **haptic** and
**audio** channels.
